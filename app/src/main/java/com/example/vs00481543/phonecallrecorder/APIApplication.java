package com.example.vs00481543.phonecallrecorder;


import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Multipart;
import retrofit2.http.Part;

public interface APIApplication {

    @Multipart
    @POST("Api.php?apicall=call_log")
    Call<CallDetails> postLog(
                            // @Field("ids") int serial,
                            @Part("ids") RequestBody ids,
                            @Part("phone_no") RequestBody phoneNumber,
                            @Part("time") RequestBody time,
                            @Part("date") RequestBody date,
                            @Part("customer_no") RequestBody customer_no,
                            @Part("file_name") RequestBody fileName,
                            //    @Field("phone_no") String num,
                            //    @Field("time") String time,
                            //    @Field("date") String date,
                            //    @Field("customer_no") String customer_no,
                            //    @Field("file_name") String file);
                               @Part MultipartBody.Part myfile);
                           //    @Part("myfile") RequestBody fileRecord);
                               
}