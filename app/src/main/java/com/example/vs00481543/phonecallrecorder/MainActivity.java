package com.example.vs00481543.phonecallrecorder;

import android.Manifest;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    public static Context c;

    JobScheduler mJobScheduler;

    DatabaseHandler db=new DatabaseHandler(this);
    final static String TAGMA="Main Activity";
    RecordAdapter rAdapter;
    RecyclerView recycler;
    List<CallDetails> callDetailsList;
    boolean checkResume=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /*StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());*/

      /*  if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }*/

        SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(this);
        pref.edit().putInt("numOfCalls",0).apply();



        mJobScheduler = (JobScheduler)
        getSystemService(Context.JOB_SCHEDULER_SERVICE);
JobInfo.Builder builder = new JobInfo.Builder(1,
        new ComponentName(getPackageName(),
                JobSchedulerService.class.getName()));
builder.setPeriodic(3000);
builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);

if (mJobScheduler.schedule(builder.build()) <= 0) {
    Log.e(TAGMA, "onCreate: Some error while scheduling the job");
}

       // pref.edit().putInt("serialNumData", 1).apply();

        //rAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("Check", "onResume: ");
        if(checkPermission()) {
            Toast.makeText(getApplicationContext(), "Permission already granted", Toast.LENGTH_LONG).show();
            if(checkResume==false) {
                setUi();
                // this.callDetailsList=new DatabaseManager(this).getAllDetails();
                rAdapter.notifyDataSetChanged();
            }
        }
    }

    protected void onPause()
    {
        super.onPause();
        SharedPreferences pref3=PreferenceManager.getDefaultSharedPreferences(this);
        if(pref3.getBoolean("pauseStateVLC",false)) {
            checkResume = true;
            pref3.edit().putBoolean("pauseStateVLC",false).apply();
        }
        else
            checkResume=false;
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.mainmenu,menu);
        MenuItem item=menu.findItem(R.id.mySwitch);

        View view = getLayoutInflater().inflate(R.layout.switch_layout,null,false) ;

        final SharedPreferences pref1= PreferenceManager.getDefaultSharedPreferences(this);

        SwitchCompat switchCompat = (SwitchCompat) view.findViewById(R.id.switchCheck);
        switchCompat.setChecked(pref1.getBoolean("switchOn",true));
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    Log.d("Switch", "onCheckedChanged: " +isChecked);
                    Toast.makeText(getApplicationContext(), "Call Recorder ON", Toast.LENGTH_LONG).show();
                    pref1.edit().putBoolean("switchOn",isChecked).apply();
                }else{
                    Log.d("Switch", "onCheckedChanged: " +isChecked);
                    Toast.makeText(getApplicationContext(), "Call Recorder OFF", Toast.LENGTH_LONG).show();
                    pref1.edit().putBoolean("switchOn",isChecked).apply();
                }
            }
        });
        item.setActionView(view);
        return true;
    }

    public void setUi()
    {
        recycler=(RecyclerView) findViewById(R.id.recyclerView);
        callDetailsList=new DatabaseManager(this).getAllDetails();

        for(CallDetails cd:callDetailsList)
        {
            String log="Phone num : "+cd.getNum()+" | Serial : "+cd.getSerial()+" | Time : "+cd.getTime1()+" | Date : "+cd.getDate1()+" | Status : "+cd.getStatus() + " | Sim : " + cd.getCustomer_no() + " | File : " + cd.getFile();
            Log.d("Database ", log);

            Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();

            APIApplication apiApplication = retrofit.create(APIApplication.class);
            callAPI(apiApplication, cd);
            
        }

        Collections.reverse(callDetailsList);
        rAdapter=new RecordAdapter(callDetailsList,this);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
        recycler.setLayoutManager(layoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(rAdapter);

    }


    private void callAPI(APIApplication apiApplication, final com.example.vs00481543.phonecallrecorder.CallDetails log) {

        // Log.d("Datab ", log.getFile());

        MediaType MEDIA_TYPE = MediaType.parse("audio/*");
        String mediaPath = Environment.getExternalStorageDirectory() + "/My Records/" + log.getDate1() + "/" + log.getFile();

        Log.d("Datab ", mediaPath);

        // File fileRecord = new File(mediaPath);
        // RequestBody requestBody = RequestBody.create(MEDIA_TYPE, fileRecord);

        // MultipartBody.Part multipartBody =MultipartBody.Part.createFormData("myfile",fileRecord.getName(),requestBody);

        String Serial = Integer.toString(log.getSerial());

        RequestBody UserId = RequestBody.create(MediaType.parse("multipart/form-data"), Serial);
        RequestBody phoneNumber = RequestBody.create(MediaType.parse("multipart/form-data"), log.getNum());
        RequestBody time = RequestBody.create(MediaType.parse("multipart/form-data"), log.getTime1());
        RequestBody date = RequestBody.create(MediaType.parse("multipart/form-data"), log.getDate1());
        RequestBody customer_no = RequestBody.create(MediaType.parse("multipart/form-data"), log.getCustomer_no());
        RequestBody fileName = RequestBody.create(MediaType.parse("multipart/form-data"), log.getFile());

            MultipartBody.Part profilePic=null;
            
              if(mediaPath!=null) {
                File file = new File(mediaPath);
                RequestBody requestFile = RequestBody.create( MediaType.parse("multipart/form-data"), file);
                profilePic = MultipartBody.Part.createFormData("myfile", file.getName(), requestFile);
            }

            // Call<CallDetails>call=apiApplication.postLog(UserId, profilePic, phoneNumber, time, date, customer_no, fileName);
            Call<CallDetails>call=apiApplication.postLog(UserId, phoneNumber, time, date, customer_no, fileName, profilePic);
            call.enqueue(new Callback<CallDetails>(){
                    @Override
                    public void onResponse(Call<CallDetails> call, Response<CallDetails> response) {
                        // try {
                        //     Realm realm = Realm.getDefaultInstance();
                        //     realm.beginTransaction();

                        //     // log.setSync(true);
                        //     realm.commitTransaction();
                        //     realm.close();
                        // } catch (Exception e) {
                        //     e.printStackTrace();
                        // }
                        System.out.println("result - "+ response.body());
                    }

                    @Override
                    public void onFailure(Call<CallDetails> call, Throwable t) {
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }    


    private boolean checkPermission()
    {
        int i=0;
        String[] perm={Manifest.permission.READ_PHONE_STATE,Manifest.permission.RECORD_AUDIO,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_CONTACTS};
        List<String> reqPerm=new ArrayList<>();

        for(String permis:perm) {
            int resultPhone = ContextCompat.checkSelfPermission(MainActivity.this,permis);
            if(resultPhone== PackageManager.PERMISSION_GRANTED)
                i++;
            else {
                reqPerm.add(permis);
            }
        }

        if(i==5)
            return true;
        else
            return requestPermission(reqPerm);
    }



    private boolean requestPermission(List<String> perm)
    {
        // String[] permissions={Manifest.permission.READ_PHONE_STATE,Manifest.permission.RECORD_AUDIO,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE};

        String[] listReq=new String[perm.size()];
        listReq=perm.toArray(listReq);
        for(String permissions:listReq) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,permissions)) {
                Toast.makeText(getApplicationContext(), "Phone Permissions needed for " + permissions, Toast.LENGTH_LONG);
            }
        }

        ActivityCompat.requestPermissions(MainActivity.this, listReq, 1);


        return false;
    }


    public void onRequestPermissionsResult(int requestCode,String permissions[],int[] grantResults)
    {
        switch(requestCode)
        {
            case 1:
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(getApplicationContext(),"Permission Granted to access Phone calls",Toast.LENGTH_LONG);
                else
                    Toast.makeText(getApplicationContext(),"You can't access Phone calls",Toast.LENGTH_LONG);
                break;
        }

    }

}
